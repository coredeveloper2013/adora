#Table Name: category and items
        ALTER TABLE `categories` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `name`;
        ALTER TABLE `items` ADD `slug` VARCHAR(255) NULL DEFAULT NULL AFTER `name`


#Name: orders
        ALTER TABLE `orders` ADD `promo_code_id` int(11) NULL DEFAULT NULL AFTER `billing_phone`;
               
        