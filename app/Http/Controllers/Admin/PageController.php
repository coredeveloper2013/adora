<?php

namespace App\Http\Controllers\Admin;

use App\Enumeration\PageEnumeration;
use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($id) {
        $page = Page::where('page_id', $id)->first();

        if (!$page) {
            $page = Page::create([
                'page_id' => $id,
            ]);
        }

        $title = 'Page - ';

        if ($id == PageEnumeration::$ABOUT_US)
            $title .= 'About Us';
        else if ($id == PageEnumeration::$CONTACT_US)
            $title .= 'Contact Us';
        else if ($id == PageEnumeration::$PRIVACY_POLICY)
            $title .= 'Privacy Policy';
        else if ($id == PageEnumeration::$RETURN_INFO)
            $title .= 'Return Info';
        else if ($id == PageEnumeration::$BILLING_SHIPPING_INFO)
            $title .= 'Billing & Shipping Info';

        else if ($id == PageEnumeration::$LOGIN_SNIPPET)
            $title .= 'Login snippet';


        return view('admin.dashboard.page.index', compact('page'))->with('page_title', $title);
    }

    public function save(Request $request, $id) {
        Page::where('page_id', $id)->update([
            'content' => $request->page_editor,
        ]);

        return redirect()->back()->with('message', 'Updated!');
    }
}
