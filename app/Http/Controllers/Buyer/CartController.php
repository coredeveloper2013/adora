<?php

namespace App\Http\Controllers\Buyer;

use App\Model\CartItem;
use App\Model\Coupon;
use App\Model\Item;
use App\Model\MetaVendor;
use App\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CartController extends Controller
{
    public function addToCart(Request $request) {
        $item = Item::where('status', 1)->where('id', $request->itemId)->first();

        $itemOrder = 0;

        for ($i=0; $i < sizeof($request->qty); $i++)
            $itemOrder += (int) $request->qty[$i];

        if ($itemOrder < $item->min_qty)
            return response()->json(['success' => false, 'message' => 'Minimum quantity is : '. $item->min_qty]);

        // if ( $request->total_price < $item->min_order ) {
        //     return response()->json(['success' => false, 'message' => 'Minimum order amount is : '. $item->min_order]);
        // }

        for ($i=0; $i < sizeof($request->colors); $i++) {
            $cartItem = CartItem::where([
                ['user_id', Auth::user()->id],
                ['item_id', $request->itemId],
                ['color_id', $request->colors[$i]]
            ])->first();

            if ($cartItem) {
                $cartItem->quantity += (int) $request->qty[$i];
                $cartItem->save();
            } else {
                CartItem::create([
                    'user_id' => Auth::user()->id,
                    'item_id' => $request->itemId,
                    'color_id' => $request->colors[$i],
                    'quantity' => $request->qty[$i],
                ]);
            }
        }

        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function addToCartSuccess() {
        return back()->with('message', 'Added to cart.');
    }

    public function showCart() {
        $temp = [];
        $cartItems = [];
        $vendor = MetaVendor::where('id', 1)->first();

        $cartObjs = CartItem::where('user_id', Auth::user()->id)
            ->with('item', 'color')
            ->get();

        foreach($cartObjs as $obj) {
            $temp[$obj->item->id][] = $obj;
        }

        $itemCounter = 0;

        foreach($temp as $itemId => $item) {
            $cartItems[$itemCounter] = $item;
            $itemCounter++;
        }

        $count = [];

        foreach ($cartItems as $item_index => $items) {;
            foreach($items as $item){
                $itemInPack = 0;

                $sizes = explode("-", $items[0]->item->pack->name);

                for($i=1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack'.$i;

                    if ($items[0]->item->pack->$var != null)
                        $itemInPack += (int) $items[0]->item->pack->$var;
                }


                $count[] = $item->quantity * $item->item->price * $itemInPack;
            }

        }


        $subTotal = array_sum($count);




        $discount = 0;
        if ( session()->has('promo') ) {
            $promoType = session('promo')[0]['type'];
            $promo_amount = session('promo')[0]['amount'];
            $promo = 0;
            if ( $promoType == 1 ) { // Fixed amount will be deduct
                $promo = $promo_amount;
            }
            elseif ( $promoType == 2 ) { // Percentage amount will be deduct
                $promo = ( $subTotal * $promo_amount ) / 100;
            }
            else {
               // $promo = $shipping_methods->fee;
            }
            $discount = $promo;

        }

        session(['discount' => $discount]);

        //return 990 - 39.6;

        //return session('discount');
        //return $discount;
        //return session('promo')[0]['id'];


        //return $cartItems;

        return view('pages.cart', compact('cartItems', 'vendor' , 'discount'))->with('page_title', 'Cart');
    }

    public function get_promo_price(Request $request){

        $ids = $request->ids;

        $changed_values = explode(',' , $ids);
        $temp = [];
        $cartItems = [];

        $cartObjs = CartItem::where('user_id', Auth::user()->id)
            ->with('item', 'color')
            ->get();

        $another_counter = 0;
        foreach($cartObjs as $obj) {
            $obj->quantity = $changed_values[$another_counter++];
            $temp[$obj->item->id][] = $obj;
        }



        $itemCounter = 0;

        foreach($temp as $itemId => $item) {

            $cartItems[$itemCounter] = $item;
            $itemCounter++;
        }

        $count = [];

        //return $cartItems;

        foreach ($cartItems as $item_index => $items) {;
            foreach($items as $item){
                $itemInPack = 0;

                $sizes = explode("-", $items[0]->item->pack->name);

                for($i=1; $i <= sizeof($sizes); $i++) {
                    $var = 'pack'.$i;

                    if ($items[0]->item->pack->$var != null)
                        $itemInPack += (int) $items[0]->item->pack->$var;
                }


                $count[] = $item->quantity * $item->item->price * $itemInPack;
            }

        }


        $subTotal = array_sum($count);




        $discount = 0;
        if ( session()->has('promo') ) {
            $promoType = session('promo')[0]['type'];
            $promo_amount = session('promo')[0]['amount'];
            $promo = 0;
            if ( $promoType == 1 ) { // Fixed amount will be deduct
                $promo = $promo_amount;
            }
            elseif ( $promoType == 2 ) { // Percentage amount will be deduct
                $promo = ( $subTotal * $promo_amount ) / 100;
            }
            else {
                // $promo = $shipping_methods->fee;
            }
            $discount = $promo;

        }

        session(['discount' => $discount]);

        return $discount;
    }

    public function updateCart(Request $request) {
        //$cartItems = CartItem::whereIn('id', $request->ids)->with('item')->get();

        //return $request->all();

        $data = [];
        for($i=0; $i < sizeof($request->ids); $i++) {
            $ci = CartItem::where('id', $request->ids[$i])->first();

            $c = 0;

            if (isset($data[$ci->item->id]))
                $c = $data[$ci->item->id];

            $data[$ci->item->id] = (int) ($request->qty[$i]) + $c;
        }

        foreach ($data as $itemId => $q) {
            $item = Item::where('id', $itemId)->first();

            if ($item->min_qty > $q)
                return response()->json(['success' => false, 'message' => $item->style_no.' minimum order qty is '. $item->min_qty]);
        }

        for($i=0; $i < sizeof($request->ids); $i++) {
            if ($request->qty[$i] == '0')
                CartItem::where('id', $request->ids[$i])->delete();
            else {
                CartItem::where('id', $request->ids[$i])->update(['quantity' => $request->qty[$i]]);
            }
        }

        return response()->json(['success' => true, 'message' => 'Success']);
    }

    public function updateCartSuccess() {
        return back()->with('message', 'Cart Updated!');
    }

    public function deleteCart(Request $request) {
        CartItem::where('id', $request->id)->delete();
    }

    public function deleteCartAll(Request $request) {
        CartItem::where([])->delete();
    }

    public function addToCartPromo(Request $request)
    {
        $input = $request->input();

        // $check = PromoCodes::where('name', $input['code'])->get()->first();
        $check = Coupon::where('name', $input['code'])->get()->first();
        if ( $check == null ) {
            session()->forget('promo');
            return redirect()->back()->withErrors(['error' => 'Invalid Promo Code']);
        }
        if ( $check->multiple_use == 1 ) {
            session()->forget('promo');
            session()->push('promo', $check->toArray());
            return redirect()->back()->withErrors(['message' =>  'Promo code successfully added']);
        }
        else {
            // Here Promo code should apply one time
            // Check this user has used this promo
            $checkPromoUsed = Order::where('user_id', Auth::user()->id)->where('promo_code_id', $check->id)->get();
            if ( $checkPromoUsed->isEmpty() ) {
                session()->forget('promo');
                // So he has not used this promo code
                session()->push('promo', $check->toArray());
                return redirect()->back()->withErrors(['message' =>  'Promo code successfully added']);
            }
            else {
                // User has used this promo code
                return redirect()->back()->withErrors(['error' => 'You have Used This Promo Code Once!']);
            }
        }
    }
}
