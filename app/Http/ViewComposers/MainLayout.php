<?php

namespace App\Http\ViewComposers;

use App\Enumeration\PageEnumeration;
use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\CartItem;
use App\Model\Category;
use App\Model\DefaultCategory;
use App\Model\Item;
use App\Model\Meta;
use App\Model\MetaVendor;
use App\Model\Notification;
use App\Model\Page;
use App\Model\SocialLinks;
use App\Model\VendorImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Route;
use DB;

class MainLayout
{
    private $text;

    public function __construct(Request $request)
    {
        if (Route::currentRouteName() == 'category_page') {
            $this->text = $request->category;
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // Default Categories
        $defaultCategories = [];
        $categoriesCollection = Category::orderBy('sort')->orderBy('name')->get();

        foreach($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'slug' => $cc->slug
                ];

                $subCategories = [];
                foreach($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'slug' => $item->slug
                        ];

                        $data3 = [];
                        foreach($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name,
                                    'slug' => $item2->slug
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        // Cart
        $cartItems['total'] = 0;

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {

            $items = CartItem::where('user_id', Auth::user()->id)
                ->with('item')->get();
            $totalPrice = 0;
            $totalQty = 0;

            foreach($items as $item) {
                $qty = $item->quantity;
                $totalPrice += $item->quantity * $item->item->price;
                $totalQty += $item->quantity;

                if (array_key_exists($item->item->id, $cartItems))
                    $qty = (int) $cartItems[$item->item->id]['qty'] + (int) $qty;

                $image_path = asset('images/no-image.png');

                if (sizeof($item->item->images) > 0)
                    $image_path = asset($item->item->images[0]->thumbs_image_path);

                $cartItems[$item->item->id] = [
                    'name' => $item->item->style_no,
                    'qty' => $qty,
                    'image_path' => $image_path,
                    'price' => $item->item->price,
                    'details_url' => route('item_details_page', ['item' => $item->item->id])
                ];
            }

            $cartItems['total'] = [
                'total_price' => $totalPrice,
                'total_qty' => $totalQty
            ];
        }

        // Notification
        $notifications = [];

        if (Auth::check() && Auth::user()->role == Role::$BUYER)
            $notifications = Notification::where('user_id', Auth::user()->id)
                ->where('view', 0)->get();

        // Meta
        $meta_title = config('app.name');
        $meta_description = '';
        $meta = null;

        if (Route::currentRouteName() == 'home') {
            $meta = Meta::where('page', PageEnumeration::$HOME)->first();
        }  else if (Route::currentRouteName() == 'about_us') {
            $meta = Meta::where('page', PageEnumeration::$ABOUT_US)->first();
        } else if (Route::currentRouteName() == 'contact_us') {
            $meta = Meta::where('page', PageEnumeration::$CONTACT_US)->first();
        } else if (Route::currentRouteName() == 'privacy_policy') {
            $meta = Meta::where('page', PageEnumeration::$PRIVACY_POLICY)->first();
        } else if (Route::currentRouteName() == 'return_info') {
            $meta = Meta::where('page', PageEnumeration::$RETURN_INFO)->first();
        } else if (Route::currentRouteName() == 'billing_shipping') {
            $meta = Meta::where('page', PageEnumeration::$BILLING_SHIPPING_INFO)->first();
        } else if (Route::currentRouteName() == 'large_quantities') {
            $meta = Meta::where('page', PageEnumeration::$LARGE_QUANTITIES)->first();
        } else if (Route::currentRouteName() == 'refunds') {
            $meta = Meta::where('page', PageEnumeration::$REFUNDS)->first();
        } else if (Route::currentRouteName() == 'category_page') {
            foreach ($defaultCategories as $cat) {
                if (changeSpecialChar($cat['name']) == $this->text) {
                    $meta = Meta::where('category', $cat['id'])->first();
                    break;
                }
            }
        }

        if ($meta) {
            if ($meta->title != NULL && $meta->title != '')
                $meta_title = $meta->title;

            if ($meta->title != NULL)
                $meta_description = $meta->description;
        }

        // Logo Path
        /*$logo_path = '';
        $vendorLogo = VendorImage::where('status', 1)
            ->where('type', VendorImageType::$LOGO)
            ->first();

        if ($vendorLogo)
            $logo_path = asset($vendorLogo->image_path);*/
        $black_logo_path = '';
        $white_logo_path = '';

        $white = DB::table('settings')->where('name', 'logo-white')->first();
        if ($white)
            $white_logo_path = asset($white->value);

        $black = DB::table('settings')->where('name', 'logo-black')->first();
        if ($black)
            $black_logo_path = asset($black->value);

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);


        $banner_images = VendorImage::where('type', VendorImageType::$PROMO_BANNER)
            ->orderBy('sort')
            ->first();

        $notification_text = Page::where('page_id', PageEnumeration::$NOTIFICATION)->first();

        $socialLinks = SocialLinks::first();

        $view->with([
            'default_categories' => $defaultCategories,
            'cart_items' => $cartItems,
            'notifications' => $notifications,
            'meta_title' => $meta_title,
            'white_logo_path' => $white_logo_path,
            'black_logo_path' => $black_logo_path,
            'meta_description' => $meta_description,
            'defaultItemImage_path' => $defaultItemImage_path,
            'banner_images' => $banner_images,
            'notification_text' => $notification_text,
            'social_links' => $socialLinks
        ]);
    }
}