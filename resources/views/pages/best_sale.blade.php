<?php
use App\Enumeration\Availability;
use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
@stop

@section('content')
    <section class="product_page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 show_desktop">
                    <div class="product_page_left_area">
                        <div class="product_page_left_menu">
                            <h2>CATEGORY</h2>
                            <ul>
                                @foreach($categories as $category)
                                    <li class="active"><a href="#" id="category-checkbox-{{$category->id}}" class="checkbox-category"  data-id="{{ $category->id }}" {{ request()->get('C') == $category->id ? 'checked' : '' }} >{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 product_page_c_mobile">
                    <div class="main_product_area">
                        <div class="product_filter_mobile show_mobile">
                            <div class="dropdown l_drop_mobile">
                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span>Crop Tops</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <div class="product_page_left_menu">
                                        <h2>TRENDING NOW</h2>
                                        <ul>
                                            <li class="active"><a href="#">Bodysuits</a></li>
                                            <li><a href="#">Crop Tops</a></li>
                                            <li><a href="#">Mesh</a></li>
                                            <li><a href="#">Statement Sleeves</a></li>
                                            <li><a href="#">Stripes</a></li>
                                            <li><a href="#">Tube Top</a></li>
                                            <li><a href="#">Velvet</a></li>
                                        </ul>
                                        <h2>BY STYLE</h2>
                                        <ul>
                                            <li><a href="#">Floral</a></li>
                                            <li><a href="#">Graphic Tees</a></li>
                                            <li><a href="#">Lace + Embroidery</a></li>
                                            <li><a href="#">Lace-Up</a></li>
                                            <li><a href="#">Off The Shoulder</a></li>
                                            <li><a href="#">Plaids + Flannels</a></li>
                                            <li><a href="#">Shirts + Blouses</a></li>
                                            <li><a href="#">Sweaters + Knits</a></li>
                                            <li><a href="#">Sweatshirts + Hoodies</a></li>
                                            <li><a href="#">Tees + Tanks</a></li>
                                        </ul>
                                        <h2>BY OCCASION</h2>
                                        <ul class="no_border">
                                            <li><a href="#">Casual</a></li>
                                            <li><a href="#">Going Out</a></li>
                                            <li><a href="#">Work</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="m_drop_mobile">
                                <ul class="clearfix">
                                    <li>
                                        <div class="main_product_filter_dropdown">
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    SORT BY
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="#">Newest</a>
                                                    <a class="dropdown-item" href="#">Price low to high</a>
                                                    <a class="dropdown-item" href="#">Price high to low</a>
                                                    <a class="dropdown-item" href="#">Highest Rating</a>
                                                    <a class="dropdown-item" href="#">Most Popular</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="p_fiter">
                                        filter
                                    </li>
                                </ul>
                            </div>
                            <div class="filter_form">
                                <div class="filter_form_inner">
                                    <div class="product_left_filter">
                                        <div class="col-md-3 show_desktop">
                                            <div class="widget-categories">
                                                <h6 class="widget-title">CATEGORY</h6>

                                                <ul>
                                                    @foreach($categories as $category)
                                                        <li>
                                                            <div class="medi_form_group_checkbox">
                                                                <input class="custom-control-input checkbox-category filled"
                                                                       type="checkbox" id="category-checkbox-{{$category->id}}"
                                                                       data-id="{{ $category->id }}" {{ request()->get('C') == $category->id ? 'checked' : '' }}>
                                                                <label for="category-checkbox-{{$category->id}}">
                                                                    {{ $category->name }}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <h6 class="widget-title">SEARCH</h6>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input class="custom-control-input" type="radio" id="search-style-no" name="search-component" value="1"
                                                        {{ (request()->get('search-by') == '') ? 'checked' : (request()->get('search-by') == 1 ? 'checked' : '') }}>
                                                <label class="custom-control-label" for="search-style-no">Style No.</label>
                                            </div>

                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input class="custom-control-input" type="radio" id="search-description" name="search-component" value="2"
                                                        {{ request()->get('search-by') == 2 ? 'checked' : '' }}>
                                                <label class="custom-control-label" for="search-description">Description</label>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control" id="search-input" type="text" placeholder="Search"
                                                       value="{{ request()->get('search') }}">
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input class="form-control" id="search-price-min" type="text" placeholder="Price Min"
                                                           value="{{ request()->get('price_min') }}">
                                                </div>

                                                <div class="col-md-6">
                                                    <input class="form-control" id="search-price-max" type="text" placeholder="Price Max"
                                                           value="{{ request()->get('price_max') }}">
                                                </div>
                                            </div>

                                            <button class="btn btn-secondary mt-3" id="btn-search">SEARCH</button>
                                        </div>
                                        <h2>FILTER <a href="#">CLEAR ALL</a> <button class="cancel_form">DONE</button></h2>
                                        <div class="common_accordion product_left_accordion">
                                            <div id="accordion">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapse3">
                                                            SIZE
                                                        </button>
                                                    </div>
                                                    <div id="collapse3" class="collapse">
                                                        <div class="card-body clearfix">
                                                            <ul class="product_left_checkbox">
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                                                                    <label for="styled-checkbox-1">XS</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked>
                                                                    <label for="styled-checkbox-2">M</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" >
                                                                    <label for="styled-checkbox-3">XL</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4">
                                                                    <label for="styled-checkbox-4">S</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value5">
                                                                    <label for="styled-checkbox-5">L</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header">
                                                        <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapse4" >
                                                            COLOR
                                                        </button>
                                                    </div>
                                                    <div id="collapse4" class="collapse ">
                                                        <div class="card-body clearfix">
                                                            <ul class="product_left_checkbox">
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value1">
                                                                    <label for="styled-checkbox-6">black</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value2" checked>
                                                                    <label for="styled-checkbox-7">grey</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-8" type="checkbox" value="value3" >
                                                                    <label for="styled-checkbox-8">red</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-9" type="checkbox" value="value4">
                                                                    <label for="styled-checkbox-9">denim</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-10" type="checkbox" value="value5">
                                                                    <label for="styled-checkbox-10">pink</label>
                                                                </li>
                                                                <li>
                                                                    <input class="styled-checkbox" id="styled-checkbox-11" type="checkbox" value="value5">
                                                                    <label for="styled-checkbox-11">pink</label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price_range_wrapper">
                                            <h2>PRICE RANGE</h2>
                                            {{--<div class="price-range-slider"></div>--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="total_product text-center">
                                <p>160 Products</p>
                            </div>
                        </div>
                        @if (sizeof($bestItems) > 0)
                            <div class="main_product_filter show_desktop">
                                <h2>Best Sale</h2>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="main_product_filter_dropdown">
                                            <div class="dropdown">
                                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    SORT BY
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="#">Newest</a>
                                                    <a class="dropdown-item" href="#">Price low to high</a>
                                                    <a class="dropdown-item" href="#">Price high to low</a>
                                                    <a class="dropdown-item" href="#">Highest Rating</a>
                                                    <a class="dropdown-item" href="#">Most Popular</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="total_product text-center">
                                            <p class="totalItem">  </p> <p>Products </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="product_pagination">
                                            <ul>
                                                <li><a href="#"><img src="{{ asset('themes/andthewhy/images/product/icon_prev.svg')}}" alt=""></a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">...</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#"><img src="{{ asset('themes/andthewhy/images/product/icon_next.svg')}}" alt=""></a></li>
                                            </ul>
                                        </div>
                                        <div class="view_product_sorting">
                                            <div class="custom_select">
                                                <select>
                                                    <option>VIEW 30</option>
                                                    <option>VIEW 60</option>
                                                    <option>VIEW 90</option>
                                                    <option>VIEW 120</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="main_product_area">
                                <div class="row">
                                    <?php $s = (sizeof($bestItems) >= 5 ? 5 : sizeof($bestItems)); ?>

                                    @for($i=0; $i<$s; $i++)
                                        <?php $item = $bestItems[$i]; ?>
                                        <div class="col-6 col-md-4 col-lg-3 product_page_custom_padding">
                                            <div class="main_product_inner">
                                                <div class="main_product_inner_img clearfix">
                                                    <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                        @if (sizeof($item->images) > 0)
                                                            <img src="{{  (!auth()->user()) ? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}" class="img-fluid">
                                                        @else
                                                            <img src="{{ asset('images/no-image.png') }}" alt="Product">
                                                        @endif
                                                        @if (sizeof($item->images) > 1)
                                                            <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($item->images[1]->list_image_path) }}" class="img_hover" alt="{{ $item->name }}" class="img-fluid">
                                                            <!-- end of .hidden -->
                                                        @endif
                                                        {{-- <span data-toggle="modal" data-target="#quickview">QUICK VIEW</span>
     --}}
                                                    </a>
                                                    <div class="caption">
                                                        <div class="centered show_quick_view" data-pid="{{$item->id}}">
                                                            Quick View
                                                        </div><!-- end of .centered -->
                                                    </div>
                                                </div>
                                                <div class="main_product_inner_text clearfix">
                                                    <a href="{{ route('item_details_page', ['item' => $item->id]) }}">

                                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                            <h2 >
                                                                @if ($item->orig_price != null)
                                                                    <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                                @endif
                                                                ${{ sprintf('%0.2f', $item->price) }}

                                                            </h2>
                                                        @endif
                                                        {{--<h2>$22.90</h2>--}}
                                                        <p>{{ $item->name }}</p>
                                                    </a>
                                                    <a href="#" class="product_wishlist"><img src="images/wishlist2.svg" alt=""></a>
                                                </div>
                                                <div class="main_product_inner_button">
                                                    <ul>
                                                        @if (sizeof($item->images) > 1)
                                                            @foreach($item->images as $image)
                                                                <li class="active"><button data-pid="{{$item->id}}" data-toggle="modal" data-target="#quickViewArea" class="show_quick_view"><img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($image->list_image_path) }}" alt=""></button></li>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endfor

                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('modal')
    <div class="modal fade" id="quickViewArea" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="product-img-box">
                                <div id="fotormaPopup" class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="120" data-width="100%"></div>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="pt-1 mb-2"><span class="text-medium stock_item_preview">Stock:</span>

                            </div>
                            <div class="product-title"><h1></h1></div>
                            <div class="product-sku"><h2 class="text-normal"><span class="text-medium">Style#:</span><span class="product_style_sku"></span> </h2>
                            </div>
                            <div class="product_price"></div>

                            <div class="modal_color_table">

                            </div>

                            <div class="d-flex flex-wrap justify-content-between">
                                <div class="sp-buttons mt-2 mb-2">
                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <button class="btn btn-primary" id="btnAddToCart"><i class="icon-bag"></i> Add to Shopping Bag</button>

                                        <a href="{{ route('show_cart') }}" class="btn btn-secondary"> Checkout</a>
                                    @else
                                        <a href="/login" class="btn btn-primary">Login to Add to Cart</a>
                                    @endif
                                </div>
                            </div>

                            <input type="hidden" id="itemInPack">
                            <input type="hidden" id="itemPrice">
                            <input type="hidden" id="modalItemId">
                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                <input type="hidden" id="loggedIn" value="true">
                            @else
                                <input type="hidden" id="loggedIn" value="false">
                            @endif

                            <br>
                            <h4>Description</h4>
                            <p class="text-xs description"> Description</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalShopFilters" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filters</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <section class="widget widget-categories">
                        <h3 class="widget-title">CATEGORY</h3>
                        <ul>
                            @foreach($categories as $cat)
                                <li><a href="{{ route('category_page', ['category' => changeSpecialChar($cat->name)]) }}">{{ $cat->name }} ({{ $cat->count }})</a></li>
                            @endforeach
                        </ul>
                    </section>

                    <!-- Widget Brand Filter-->
                    {{--<section class="widget widget-categories">--}}
                    {{--<h3 class="widget-title">{{ $category->name }} VENDOR</h3>--}}
                    {{--<ul>--}}
                    {{--@foreach($vendors as $vendor)--}}
                    {{--<li><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--</section>--}}
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/fotorama.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var message = '{{ session('message') }}';

        // if (message != '')
        //     toastr.success(message);

        // WishList
        $(document).on('click', '.btnAddWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Added to Wish List.');

                $this.removeClass('btnAddWishList');
                $this.addClass('btnRemoveWishList');
                $this.html('<i class="fas fa-heart"></i>');
            });
        });

        // Click to Show Quick View Functionalities
        $(document).on('click', '.show_quick_view', function () {
            var pid = $(this).attr('data-pid');
            $.ajax({
                method: "POST",
                url: "{{ route('quick_view_item') }}",
                data: {item: pid}
            }).done(function (data) {
                if(data.item){
                    var item = data.item;
                    // var $fotoramaDiv = $('.fotorama').fotorama();
                    // var fotorama = $fotoramaDiv.data('fotorama');
                    // if(fotorama){
                    //     fotorama.destroy();
                    // }
                    if(item && item.images && item.images.length){
                        $('#itemPrice').val(item.price);
                        $('#modalItemId').val(item.id);
                        var loggedIn = $('#loggedIn').val();
                        var lPriceRow, ltPriceRow;

                        if(loggedIn && loggedIn === 'true'){
                            lPriceRow = '<span class="price">$0.00</span> <input class="input-price" type="hidden" value="0">';
                            ltPriceRow = '<b><span id="totalPrice">$0.00</span></b>';
                        }else{
                            lPriceRow = '<span class="login">$xxx</span>';
                            ltPriceRow = '<b><span id="loginrequire">$xxx</span></b>';
                        }

                        // Table Maker
                        var cTable = '<table class="table table-bordered">\n' +
                                '<thead class="bgfame">\n' +
                                '<tr>' +
                                '<th></th><th>Color</th>';

                        cTable+='<th width="20%">Min Qty</th>' +
                                '<th class="hidden-sm-down" width="20%">Qty</th>' +
                                '<th width="20%">Amount</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody class="text-sm">';

                        $.each(item.colors, function (i, color) {
                            cTable+='<tr>';

                            if (color.image != '')
                                cTable+='<td><a href="" data-index="'+color.image_index+'" class="btnThumb"><img src="'+color.image+'" style="height: 50px"></a></td>';
                            else
                                cTable+='<td></td>';

                            cTable+='<td>'+color.name+'</td>';
                            cTable+='<td>'+item.min_qty+'</td>';
                            cTable+='<td><input class="form-control pack" data-color="'+color.id+'" name="input-pack[]" type="text"></td>';
                            cTable+='<td>'+ lPriceRow +'</td>';
                            cTable+='</tr>';
                        });

                        cTable+='<tr>';
                        cTable+='<td colspan="3"><b>Total</b></td>' +
                                '<td><b><span id="totalQty">0</span></b></td>' +
                                '<td>'+ltPriceRow+'</td>';
                        cTable+='</tr>';


                        cTable+='</tbody>' +
                                '</table>';

                        $('.modal_color_table').html(cTable);


                        $('.product_style_sku').html(item.style_no);

                        if (item.price != '') {
                            if (item.orig_price) {
                                $('.product_price').html('<span class="h2 d-block"> <del>$' + parseFloat(item.orig_price).toFixed(2) + '</del> $' + parseFloat(item.price).toFixed(2) + '</span>');
                            } else {
                                $('.product_price').html('<span class="h2 d-block">$' + parseFloat(item.price).toFixed(2) + '</span>');
                            }
                        }

                        var imgData = item.images.map(function (image) {
                            return {img: image.image_path, thumb: image.thumbs_image_path, full: image.image_path};
                        });

                        // new fotorama load
                        $imgSlider = '<div id="fotormaPopup" class="fotorama" data-nav="thumbs" data-thumbwidth="80" data-thumbheight="120" data-width="100%">';
                        item.images.forEach(function (image) {
                            $imgSlider+= '<a href="'+image.image_path+'"><img src="'+image.image_path+'"></a>';
                        });
                        $imgSlider+= '</div>';
                        $('.product-img-box').html($imgSlider);

                        $('#fotormaPopup').fotorama();

                        // new fotorama load


                        // setTimeout(function () {
                        //     $('.fotorama').fotorama().data('fotorama').resize({width: '100%'});
                        // }, 1000);

                        // $.each(item.images, function (index, image) {
                        //     $('.product-img-box .fotorama').html('<a href="'+image.image_path+'"><img src="'+image.list_image_path+'"></a>');
                        // });
                        $('.product-title h1').html(item.name);
                        var itemAvailability = ['Unspecified', 'Arrives soon / Back Order', 'In Stock'];
                        $('.stock_item_preview').html('Stock: ' + itemAvailability[item.availability])
                        $('#quickViewArea .description').html(item.description)
                    }
                    setTimeout(function () {
                        $('#quickViewArea').modal('show');
                    });
                }
            });

        });

        $(document).on('click', '.btnThumb', function (e) {
            e.preventDefault();
            var index = parseInt($(this).data('index'));

            var $fotoramaDiv = $('#fotormaPopup').fotorama();
            var fotorama = $fotoramaDiv.data('fotorama');

            fotorama.show(index);
        });

        var totalQty = 0;
        $(document).on('keyup', '.pack', function () {
            var i = 0;
            var val = $(this).val();

            if (isInt(val)) {
                i = parseInt(val);

                if (i < 0)
                    i = 0;
            }

            var perPrice = $('#itemPrice').val();

            $(this).closest('tr').find('.qty').html(i);
            $(this).closest('tr').find('.price').html('$' + (i * perPrice).toFixed(2));
            $(this).closest('tr').find('.input-price').val(i * perPrice);

            calculate();

            $(this).focus();
        });
        function isInt(value) {
            return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
        }
        function calculate() {
            totalQty = 0;
            var totalPrice = 0;

            $('.pack').each(function () {
                i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                totalQty += i;
            });

            $('.input-price').each(function () {
                totalPrice += parseFloat($(this).val());
            });

            $('#totalQty').html(totalQty);
            $('#totalPrice').html('$' + totalPrice.toFixed(2));
        }
        $(document).on('click', '#btnAddToCart', function () {
            var colors = [];
            var qty = [];
            var vendor_id = '';


            if (totalQty == 0) {
                alert('Please select an item.');
                return;
            }

            var valid = true;
            $('.pack').each(function () {
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        return valid = false;
                } else {
                    if (val != '')
                        return valid = false;
                }

                if (i != 0) {
                    colors.push($(this).data('color'));
                    qty.push(i);
                }
            });

            if (!valid) {
                alert('Invalid Quantity.');
                return;
            }

            var itemId = $('#modalItemId').val();

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_cart') }}",
                data: { itemId: itemId, colors: colors, qty: qty, vendor_id: vendor_id }
            }).done(function( data ) {
                if (data.success)
                    window.location.replace("{{ route('add_to_cart_success') }}");
                else
                    alert(data.message);
            });
        });

        $(document).on('click', '.btnRemoveWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Remove from Wish List.');

                $this.removeClass('btnRemoveWishList');
                $this.addClass('btnAddWishList');

                $this.html('<i class="far fa-heart"></i>');
            });
        });
    </script>
@stop