<?php
    use App\Enumeration\Availability;
    use App\Enumeration\Role;
?>

@extends('layouts.details')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c9071f3dbd145001188ab39&product=inline-share-buttons' async='async'></script>
@stop


@section('breadcrumbs')
    {{ Breadcrumbs::render('item_details', $item) }}
@stop

@section('content')
<link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
<!-- =========================
         START BREDCRUMS SECTION
     ============================== -->
<div class="common_bredcrumbs">
    <div class="container single_product_container">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('shop_page')}}">Shop</a></li>
                        <li class="breadcrumb-item active" aria-current="page">
                            {{ $item->name }}
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- =========================
    END BREDCRUMS SECTION
============================== -->

<!-- ===============================
    START PRODUCT THUMBNAIL SECTION
=================================== -->
<section class="product_single_area">
    <div class="container single_product_container">
        <div class="row">
            <div class="col-md-6">
                <div class="single_product_slide">
                    <div id="checkoutslider" class="common_slide owl-carousel owl-theme">
                        @foreach($item->images as $img)
                            <div class="single_product_slide_inner">
                                <a href="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}">
                                    <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}" class="img-fluid">

                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="single_product_description">
                    <h2>{{ $item->name }}</h2>
            {{--        <div class="product_rating clearfix">
                        <ul>
                            <li><img src="images/single-product/star.png" alt=""></li>
                            <li><img src="images/single-product/star.png" alt=""></li>
                            <li><img src="images/single-product/star.png" alt=""></li>
                            <li><img src="images/single-product/star.png" alt=""></li>
                            <li><img src="images/single-product/star_trs.png" alt=""></li>
                            <li> (2)</li>
                        </ul>
                    </div>--}}
                    <p>
                        <b>@if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                @if ($item->orig_price != null)
                                    <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                @endif
                                ${{ sprintf('%0.2f', $item->price) }}
                            @endif
                        </b>
                    </p>
                    {{--<p><img src="{{ asset('themes/andthewhy/images/single-product/logo-afterpay.png')}}" alt="">Afterpay available for orders between $35 - $1000 Learn more</p>--}}
                    <!-- <ul class="link_to_slide">
                        <li><button><img src="images/smp2.jpg" alt="" class="img-fluid"></button></li>
                        <li><button><img src="images/smp2.jpg" alt="" class="img-fluid"></button></li>
                        <li><button><img src="images/smp2.jpg" alt="" class="img-fluid"></button></li>
                    </ul>
                    <p>WHITE/BLACK</p>
                    <ul class="product_size_desc">
                        <li class="active"><button>S</button></li>
                        <li><button>M</button></li>
                        <li><button>L</button></li>
                    </ul> -->
                    <div class="single_product_desc">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Color</th>
                                <th>{{ join(' ', $sizes) }}</th>
                                <th>Pack</th>
                                <th>Qty</th>
                                <th>{{ Auth::check() && Auth::user()->role == Role::$BUYER ? 'Price' : '' }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item->colors as $color)
                                <tr>
                                    <td>{{ $color->name }}</td>

                                    @if ($color->pivot->available ==1)
                                        <td>
                                            @for ($i = 1; $i <= sizeof($sizes); $i++)
                                                <?php $p = 'pack'.$i; ?>
                                                {{ ($item->pack->$p != null) ? $item->pack->$p : 0 }}
                                            @endfor
                                        </td>

                                        <td>
                                            <input type="text" class="form-control pack_page" data-color="{{ $color->id }}" name="input-pack[]">
                                        </td>
                                        <td class="hidden-sm-down"><span class="qty">0</span></td>

                                        <td>
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <span class="price">$0.00</span>
                                                <input type="hidden" class="input-price" value="0">
                                            @else
                                                <span></span>
                                            @endif
                                        </td>
                                    @else
                                        <td class="text-center" colspan="3">Out of Stock</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="fit_rating clearfix">
                        <h2>FIT RATING</h2>
                        <div class="fit_rating_row">
                            <span></span>
                        </div>
                        <ul>
                            <li>runs small</li>
                            <li>true to size</li>
                            <li>runs large</li>
                        </ul>
                    </div> -->
                    <p>
                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                <button id="btnAddToCart_single_page"  class="add_cart_btn" >Add to cart</button>
                            @else
                                <a class="add_cart_btn btn btn-primary"  href="{{ route('buyer_login') }}">Login to Add to Cart</a>
                            @endif

                    </p>
                    <p class="color_red">Free shipping when you add a dress to cart! Use Code: FREESHIP</p>
                    <div class="single_desc_social">

                        <ul class="clearfix">
                            <li>
                                <a href="#" id="btnAddToWishlist">
                                    <span>
                                        <i class="far fa-heart fa-2x {{ in_array($item->id, $buyer_list_items_updated) ? 'added_to_wishlist' : '' }}" id="wishlist_heart"></i>
                                    </span>
                                    <span id="wishlist_text">
                                        {{ in_array($item->id, $buyer_list_items_updated) ? 'Added' : 'Add' }} to wishlist
                                    </span>
                                    <input id="product_id" value="{{$item->id}}" type="hidden" >
                                </a>
                            </li>
                            <li><a href="#"  data-toggle="modal" data-target="#videoModal" data-theVideo="{{ $item->youtube_url }}">
                                    <img src="{{ asset('themes/andthewhy/images/single-product/icon_video.svg') }}" alt="">
                                    <span>Video</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="ic_share"><img src="{{ asset('themes/andthewhy/images/single-product/icon_share.svg') }}" alt="">
                                    <span>Share</span>
                                </a>
                            </li>
                        </ul>
                        {{--youtube_url--}}
                        <div class="youtube_url">
                            <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
                                <div class="modal-dialog custom-modal-dialog modal-lg">
                                    <div class="modal-content custom-modal-content">
                                        <div class="modal-body">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <div>
                                                @if ( ! is_null($item->youtube_url) )
                                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/{{ $item->youtube_url }}"></iframe>
                                                @else
                                                <h3>No Video Found</h3>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--youtube_url--}}
                    </div>

                    <div class="single_product_social_inner">
                        {{--<ul>--}}
                            {{--<li><a href="#"><i class="fab fa-instagram"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fab fa-twitter"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fab fa-youtube"></i></a></li>--}}
                            {{--<li><a href="#"><i class="fab fa-pinterest"></i></a></li>--}}
                        {{--</ul>--}}
                        <div class="sharethis-inline-share-buttons"></div>
                    </div>
                    <div class="common_accordion single_product_accordion">
                        <div id="INFORMATION">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn-link" data-toggle="collapse" data-target="#INFO" >
                                        PRODUCT INFORMATION
                                    </button>
                                </div>
                                <div id="INFO" class="collapse show">
                                    <div class="card-body clearfix">
                                        <h2>Details</h2>
                                        <p>Style No: {{ $item->style_no }}</p>
                                        <?php if ( ! is_null($item->fabric) ) : ?>
                                        <div>
                                            <span>Fabric: </span><label>{{ $item->fabric }}</label>
                                        </div>
                                        <?php endif; ?>
                                        <p>
                                            {!! nl2br($item->description) !!}
                                        </p>
                                        {{-- <p class="single_product_accordion_custom_p">Product Code : 2000282882</p> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn-link" data-toggle="collapse" data-target="#INFORMATIONOne">
                                        SIZE GUIDE
                                    </button>
                                </div>
                                <div id="INFORMATIONOne" class="collapse">
                                    <div class="card-body clearfix">
                                        <div class="single_product_tab">
                                            <ul class="nav nav-tabs" id="myTab1" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">IN</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">CM</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="myTabContent1">
                                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                    <table class="size_guide_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Size</th>
                                                            <th>Bust</th>
                                                            <th>Waist</th>
                                                            <th>Hips</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>XS</td>
                                                            <td>81</td>
                                                            <td>61 - 64</td>
                                                            <td>84 - 86</td>
                                                        </tr>
                                                        <tr>
                                                            <td>S</td>
                                                            <td>86 - 89 </td>
                                                            <td>66 - 69 </td>
                                                            <td>89 - 92</td>
                                                        </tr>
                                                        <tr>
                                                            <td>M</td>
                                                            <td>91 - 94</td>
                                                            <td>71 - 74</td>
                                                            <td>96 - 102</td>
                                                        </tr>
                                                        <tr>
                                                            <td>L</td>
                                                            <td>96 - 99</td>
                                                            <td>76 - 79</td>
                                                            <td>107 - 112</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                    <table class="size_guide_table">
                                                        <thead>
                                                        <tr>
                                                            <th>Size</th>
                                                            <th>Bust</th>
                                                            <th>Waist</th>
                                                            <th>Hips</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>XS</td>
                                                            <td>81</td>
                                                            <td>61 - 64</td>
                                                            <td>84 - 86</td>
                                                        </tr>
                                                        <tr>
                                                            <td>S</td>
                                                            <td>86 - 89 </td>
                                                            <td>66 - 69 </td>
                                                            <td>89 - 92</td>
                                                        </tr>
                                                        <tr>
                                                            <td>M</td>
                                                            <td>91 - 94</td>
                                                            <td>71 - 74</td>
                                                            <td>96 - 102</td>
                                                        </tr>
                                                        <tr>
                                                            <td>L</td>
                                                            <td>96 - 99</td>
                                                            <td>76 - 79</td>
                                                            <td>107 - 112</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<p class="t_d_u text-right">View Details</p>--}}
                </div>
            </div>
            <div class="col-md-1 order-md-first">
                <ul class="single_product_left_thumbnail">
                    @php
                            $image_count = 1;
                            @endphp
                    @foreach($item->images as $img)
                        <li>
                            <img id="owlimage-{{$image_count}}" src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path) }}" class="owlimage img-fluid">
                        </li>
                        @php
                            $image_count++;
                        @endphp
                    @endforeach

                </ul>

            </div>
        </div>
    </div>
</section>
<!-- ===============================
    END PRODUCT THUMBNAIL SECTION
=================================== -->

<!-- =========================================
    START WHY DID I CHOOSE SECTION
========================================== -->
@if(count($item->reviews) > 0)
<section class="why_choose_area">
    <div class="container single_product_container">
        <div class="row">
            <div class="col-lg-12">
                <div class="why_choose_title text-center">
                    <h2>WHAT CUSTOMER SAYS ABOUT THIS PRODUCT?</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @php
                    $review_count = 0
                    @endphp
            @foreach($item->reviews as $r)
                @if ($review_count >= 10)
                    @break
                @endif
                <div class="col-md-6 custom_padding_md">
                    <div class="why_choose_inner">

                            <div class="customer_rateYo" data-type="{{ $r->rating }}"></div>

                        <p>{{ $r->comment  }}</p>
                        <h2>{{ $r->user->first_name  }} <span></span></h2>
                    </div>
                </div>
                    @php
                        $review_count++;
                    @endphp
            @endforeach

            {{--<div class="col-md-6 custom_padding_md">--}}
                {{--<div class="why_choose_inner">--}}
                    {{--<p>Love the length</p>--}}
                    {{--<h2>Nafika I., <span>Purchased 01/15/2019</span></h2>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 custom_padding_md">--}}
                {{--<div class="why_choose_inner">--}}
                    {{--<p>Love the length</p>--}}
                    {{--<h2>Nafika I., <span>Purchased 01/15/2019</span></h2>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 custom_padding_md">--}}
                {{--<div class="why_choose_inner">--}}
                    {{--<p>Love the length</p>--}}
                    {{--<h2>Nafika I., <span>Purchased 01/15/2019</span></h2>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>

        {{--<div class="row">--}}
            {{--<div class="col-lg-12">--}}
                {{--<div class="why_choose_read_more text-right">--}}
                    {{--<p><span>Show more</span></p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</section>
@endif
<!-- =========================================
    END WHY DID I CHOOSE SECTION
========================================== -->

<!-- =========================================
    START PRODCUT THUMB WITH TEXT SECTION
========================================== -->
<section class="product_thumb_with_text_area related_product">
    <div class="heading_with_arrow text-center">
        <h3>YOU MIGHT ALSO LIKE</h3>
    </div>
    <div class="container single_product_container">
        <div class="row">
            <div class="col-lg-12">
                <div id="similiar_item_slide2" class="common_slide owl-carousel owl-theme">
                    {{--@foreach($suggestItems as $suggestItem)--}}
                        {{--<div class="related_product_inner">--}}
                            {{--<a href="{{ route('item_details_page', ['item' => $suggestItem->slug]) }}">--}}
                                {{--@if (sizeof($suggestItem->images) > 0)--}}
                                    {{--<img src="{{(!auth()->user()) ? $defaultItemImage_path : asset($suggestItem->images[0]->list_image_path) }}"--}}
                                         {{--alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">--}}
                                {{--@else--}}
                                    {{--<img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">--}}
                                {{--@endif--}}
                            {{--</a>--}}

                            {{--<p>{{ $suggestItem->name }}</p>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}
                        @foreach($suggestItems as $suggestItem)
                        <div class="product_with_text_inner">
                        <a href="{{ route('item_details_page', ['item' => $suggestItem->slug]) }}">
                        <span class="product_with_quick_view">
                            @if (sizeof($suggestItem->images) > 0)
                                <img src="{{(!auth()->user()) ? $defaultItemImage_path : asset($suggestItem->images[0]->list_image_path) }}"
                                alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">
                            @else
                                <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $suggestItem->name }}" class="img-fluid">
                            @endif
                            <span data-toggle="modal" class="show_quick_view" data-url="{{ route('item_details_page', ['item' => $suggestItem->slug])  }}" data-pid="{{ $suggestItem->id }}" data-target="#quickview">QUICK VIEW</span>
                        </span>
                            <p class="product_title">{{ $suggestItem->name }}</p>
                        </a>
                        </div>
                       @endforeach

                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================================
    END PRODCUT THUMB WITH TEXT SECTION
========================================== -->



<!-- =========================================
    START REVIEW AND RATING SECTION
========================================== -->
<div class="single_product_rating">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single_product_rating_inner text-center">
                    <h2>REVIEW & RATING</h2>
                    {{--<p>Not yet rated. Be the first to Write a Review.</p>--}}



                    @if ( isset(Auth::user()->id) )
                        <button>WRITE REVIEW</button>
                    @else

                            <a  href="{{ route('buyer_login') }}">LOGIN TO WRITE RIVIEW</a>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="review_rating_right_menu">
    <h2>WRITE YOUR REVIEW</h2>
    <span class="close_ic"><img src="images/icon_close.png" alt=""></span>
    <form action="{{ route('item_rating', $item['id']) }}" method="POST">
        {{ csrf_field() }}
            <div class="review_title clearfix">
                @php
                $review_image = '';
                    foreach($item->images as $img){
                    $review_image = (!auth()->user()) ? $defaultItemImage_path : asset($img->image_path);
                    break;
                    }



                @endphp
                <img src="{{$review_image}}" alt="">
                <p>{{ $item->name }}</p>
                <h2>
                    <p>
                        <b>@if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                @if ($item->orig_price != null)
                                    <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                @endif
                                ${{ sprintf('%0.2f', $item->price) }}
                            @endif
                        </b>
                    </p>
                </h2>
            </div>
            <div class="clearfix"></div>
            <div class="review_description clearfix" style="margin-bottom: 10px">
                <div>Review Rating * <div id="rateYo"></div></div>

            </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
            {{--<div class="review_size_fit">--}}
                {{--<p>Size & Fit *</p>--}}
                {{--<button>Runs Small</button>--}}
                {{--<button>True to Size</button>--}}
                {{--<button>Runs Big</button>--}}
            {{--</div>--}}
            <div class="form-group common_form">
                <p>REVIEW TITLE</p>
                <input type="text" class="form-control" name="title" required>

            </div>
            <div class="form-group common_form">
                <p>WRITE REVIEW</p>
                <textarea name="comment" id="" cols="30" rows="10" class="form-control"></textarea>
                <input type="hidden" id="rate_value" name="rate_value" value="5">
                <input type="hidden" name="product_id" value="{{ $item['id'] }}">

            </div>
            <p>Please avoid using any inappropriate language, personal information, HTML, references to other retailers or copyrighted comments.</p>
            <div class="form-group common_form">
                <button>SUBMIT</button>
            </div>
    </form>
</div>
<!-- =========================================
    END REVIEW AND RATING SECTION
========================================== -->

<div class="modal fade bd-example-modal-lg common_popup_content sazzad" id="quickViewArea"
     tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg main_product_pop_up"
         role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}"
                         class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1 order-md-first product-img-box">

                        </div>

                        <div class="col-sm-6 no-padding-left">
                            {{-- <!-- <img src="{{ asset('themes/andthewhy/images/product/product-41.jpg') }}" alt="" class="img-fluid"> --> --}}
                            <div class="product_pop_up_left">
                                <div class="single_product_slide">
                                    <div id="s_pop_up_slider"
                                         class="common_slide owl-carousel owl-theme product-img-big">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 no-padding-right">
                            <div class="product_pop_up_right">
                                <div class="product-title"><h2></h2></div>
                                <p class="product_price"></p>

                                <div class="single_product_desc modal_color_table">

                                </div>
                                <p>
                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <button class="add_cart_btn" id="btnAddToCart"><i class="icon-bag"></i> Add to Shopping Bag</button>

                                        <a href="{{ route('show_cart') }}" class="add_cart_btn btn btn-secondary"> Checkout</a>
                                    @else
                                        <a href="/login" class="add_cart_btn btn btn-secondary">Login to Add to Cart</a>
                                    @endif
                                </p>
                                <div class="common_accordion product_left_accordion">
                                    <div id="INFORMATION">
                                        <div class="card">
                                            <div class="card-header">
                                                <button class="btn-link collapsed"
                                                        data-toggle="collapse"
                                                        data-target="#INFORMATIONOne">
                                                    PRODUCT INFORMATION
                                                </button>
                                            </div>
                                            <input type="hidden" id="itemInPack">
                                            <input type="hidden" id="itemPrice">
                                            <input type="hidden" id="modalItemId">
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <input type="hidden" id="loggedIn" value="true">
                                            @else
                                                <input type="hidden" id="loggedIn"
                                                       value="false">
                                            @endif
                                            <div id="INFORMATIONOne" class="collapse">
                                                <div class="card-body clearfix">
                                                    <h2>Details</h2>

                                                    <p class="text-xs description">
                                                        Description</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="text-medium stock_item_preview">Stock:</span>

                                <div class="product-sku">
                                    <span class="text-medium">Style#:</span>
                                    <span class="product_style_sku"></span>
                                </div>
                                <p class="see_full_details">
                                    <a class="itemDetails" href="{{ route('item_details_page', ['item' => $item->slug])  }}">VIEW FULL DETAILS</a>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    @include('pages.quick_item_js')
    <script>
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var is_logged_in = '{{auth()->user() ? 'yes' : 'no'}}';


            var message = '{{ session('message') }}';

            // if ( message != '' ) {
            //     console.log('called 1');
            //     //toastr.success(message);
            // }

            var itemInPack = {{ $itemInPack }};
            var perPrice = parseFloat('{{ $item->price }}');
            var totalQty = 0;
            var itemId = '{{ $item->id }}';

            $('.pack_page').keyup(function () {
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                $(this).closest('tr').find('.qty').html(itemInPack * i);
                $(this).closest('tr').find('.price').html('$' + (itemInPack * i * perPrice).toFixed(2));
                $(this).closest('tr').find('.input-price').val(itemInPack * i * perPrice);

                calculate();

                $(this).focus();
            });

            $('#btnAddToCart_single_page').click(function () {
                calculate();
                var colors = [];
                var qty = [];
                var total_price = 0;
                var vendor_id = '{{ $item->vendor_meta_id }}';

                //console.log('total totalQty :' , totalQty);

                if (totalQty === 0) {
                    alert('Please select an item.');
                    return;
                }

                var valid = true;
                $('.pack_page').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        if (val != '')
                            return valid = false;
                    }

                    if (i != 0) {
                        colors.push($(this).data('color'));
                        qty.push(i);
                    }
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                $('.input-price').each(function () {
                    total_price += parseFloat($(this).val());
                });


                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_cart') }}",
                    data: { itemId: itemId, colors: colors, qty: qty, total_price: total_price, vendor_id: vendor_id }
                }).done(function( data ) {
                    if (data.success) {
                        window.location.replace("{{ route('add_to_cart_success') }}");
                    } else {
                        alert(data.message);
                    }
                });
            });

            function calculate() {
                totalQty = 0;
                var totalPrice = 0;

                $('.qty').each(function () {
                    totalQty += parseInt($(this).html());
                });

                $('.input-price').each(function () {
                    totalPrice += parseFloat($(this).val());
                });

                $('#totalQty').html(totalQty);
                $('#totalPrice').html('$' + totalPrice.toFixed(2));
            }

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            // Wishlist
            $('#btnAddToWishlist').click(function (e) {
                e.preventDefault();
                var id = document.getElementById('product_id').value;
                $this = $(this);

                if(is_logged_in === 'no'){
                    toastr.success('You have to log in to perform this action');
                    return;
                }


                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    if(data === 'added'){
                        toastr.success('Added to Wishlist.');
                        $('#wishlist_heart').addClass('added_to_wishlist');
                        document.getElementById('wishlist_text').innerText = 'Added to wishlist'
                    } else if(data === 'removed'){
                        $('#wishlist_heart').removeClass('added_to_wishlist');
                        document.getElementById('wishlist_text').innerText = 'Add to wishlist'
                        toastr.success('Removed from Wishlist.');
                    }

                    //$this.remove();
                });
            });

            // On Back
            if (window.history && window.history.pushState) {

                $(window).on('popstate', function() {
                    localStorage['change_page'] = 1;
                    localStorage['change_pos'] = 1;
                    history.back();
                });

                window.history.pushState('forward', null, '');
            }


            $('.owlimage').click(function (e) {
                e.preventDefault();
                var order = $(this).attr('id').split('-')[1];
                $('.owl-dot:nth-child('+order+')').trigger('click')

            })

            $('.owl-dots').hide();


            $("#rateYo").rateYo({
                rating: 5,
                fullStar: true,
                onChange: function (rating, rateYoInstance) {
                    $('#rate_value').val(rating);
                }
            });

            $(".customer_rateYo").each(function(){
                var rating = $(this).attr('data-type');
                $(this).rateYo({
                    rating: rating,
                    starWidth: "17px",
                    readOnly: true,
                    normalFill: "#cfcfcf",
                    ratedFill: "#000"
                });
            });


        });
    </script>
@stop