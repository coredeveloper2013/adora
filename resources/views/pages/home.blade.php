<?php use App\Enumeration\Role; ?>
@extends('layouts.home_layout')
@section('additionalCSS')
<style>
    .slick-slide img {
        width: 100%;
    }
</style>
@stop
@section('content')
<!-- =========================
START TOP SLIDE BIG BANNER SECTION
============================== -->
@if (sizeof($mainSliderImages) > 0)
<section class="banner_area">
    <div id="main_slider" class=" common_slide owl-carousel owl-theme">
        @foreach($mainSliderImages as $banner)
            <div class="banner_item">
                @if(strpos($banner->image_path, '.mp4') !== false)
                    <video loop muted preload="metadata">
                        <source src="{{ asset($banner->image_path) }}" type="video/mp4">
                    </video>
                @else
                    <a href="{{$banner->url}}"><img src="{{ asset($banner->image_path) }}" alt=""
                                                    class="img-fluid"></a>
                @endif
            </div>
        @endforeach
    </div>
</section>
@endif
<!-- =========================
END TOP SLIDE BIG BANNER SECTION
============================== -->
<!-- ===============================
START SMALL  SLIDE  BANNER   SECTION
=================================== -->

@php
        $is_customer_logged_in = Auth::check() && Auth::user()->role == Role::$BUYER;
        @endphp
@if (sizeof($home_banners) > 0)
<section class="product_thumb_area">
    <div class="container-fluid">
        <div class="row">
            @foreach($home_banners as $v=>$home_banner)
                <div class="col-6 col-md-6 col-lg-3 custom_padding_md">
                    <div class="product_thumb_inner">
                        <a href="#">
                            @if(strpos($home_banner->image_path, '.mp4') !== false)
                                <video loop muted preload="metadata">
                                    <source src="{{ asset($home_banner->image_path) }}" type="video/mp4">
                                </video>
                            @else
                                <a href="{{$home_banner->url}}">
                                    <img src="{{ asset($home_banner->image_path) }}" alt=""
                                         class="img-fluid">
                                </a>
                            @endif
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif
<!-- ===============================
END SMALL  SLIDE  BANNER SECTION
=================================== -->
<!-- =========================================
START New Arrivals PRODCUT THUMB WITH TEXT SECTION
========================================== -->
@if (sizeof($newArrivalItems) > 0)
<section class="product_thumb_with_text_area">
    <div class="heading_with_arrow text-center">
        <h3>New Arrivals</h3>
    </div>
    <div class="common_max_width">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div id="home_product_slider1" class="common_slide owl-carousel owl-theme">
                        @foreach($newArrivalItems as $k=>$item)
                            <div class="product_with_text_inner">
                                {{-- <a href="{{ route('item_details_page', ['item' => $item->id]) }}"> --}}
                                <a href="{{ route('item_details_page', $item->slug) }}">
                                <span class="product_with_quick_view">
                                @if ( sizeof($item->images) > 0 )
                                    <img src="{{ ( ! $is_customer_logged_in ) ? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}" alt="{{ (!auth()->user()) ? config('app.name') : $item->style_no }}" class="img-fluid">
                                @else
                                        <img src="{{ ( ! $is_customer_logged_in ) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $item->style_no }}" class="img-fluid">
                                @endif
                                    <span class="show_quick_view"
                                          data-url="{{ route('item_details_page', ['item' => $item->id]) }}"
                                          data-pid="{{$item->id}}">QUICK VIEW</span>
                                </span>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                <h2 class="product_price">
                                    @if ($item->orig_price != null)
                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                    @endif
                                    ${{ sprintf('%0.2f', $item->price) }}
                                </h2>
                                @endif
                                <p class="product_title">{{ $item->name }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="product_view_more text-right">
                        <a href="{{route('new_arrival_page')}}">View More +</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- =========================================
END New Arrivals PRODCUT THUMB WITH TEXT SECTION
========================================== -->

<!-- =========================================
START Best Sellers PRODCUT THUMB WITH TEXT SECTION
========================================== -->
@if (sizeof($bestItems) > 0)
<section class="product_thumb_with_text_area">
    <div class="heading_with_arrow text-center">
        <h3>Best Sellers</h3>
    </div>
    <div class="common_max_width">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div id="home_product_slider2" class="common_slide owl-carousel owl-theme">
                        @foreach($bestItems as $k=>$bestItem)
                            <div class="product_with_text_inner">
                                <a  href="{{ route('item_details_page', ['item' => $bestItem->slug]) }}">
                                   <span class="product_with_quick_view">
                                       @if (sizeof($bestItem->images) > 0)
                                           <img src="{{ (!$is_customer_logged_in) ? $defaultItemImage_path : asset($bestItem->images[0]->list_image_path) }}"
                                                alt="{{ (!$is_customer_logged_in) ? config('app.name') : $bestItem->style_no }}"
                                                class="img-fluid">
                                       @else
                                           <img src="{{ ( ! $is_customer_logged_in ) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $bestItem->style_no }}" class="img-fluid">
                                       @endif
                                       <span class="show_quick_view"
                                             data-url="{{ route('item_details_page', ['item' => $bestItem->id]) }}"
                                             data-pid="{{$bestItem->id}}">QUICK VIEW</span>
                                    </span>
                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <h2 class="product_price">
                                            @if ($bestItem->orig_price != null)
                                                <del>
                                                    ${{ number_format($bestItem->orig_price, 2, '.', '') }}</del>
                                            @endif
                                            ${{ sprintf('%0.2f', $bestItem->price) }}
                                        </h2>
                                    @endif
                                    <p class="product_title"> {{ $bestItem->name }}</p>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="product_view_more text-right">
                        <a href="{{route('best_seller_page')}}">View More +</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- =========================================
END Best Sellers PRODCUT THUMB WITH TEXT SECTION
========================================== -->
<!-- =========================================
START Sale PRODCUT THUMB WITH TEXT SECTION
========================================== -->



@if (sizeof($saleItems) > 0)
    <section class="product_thumb_with_text_area">
        <div class="heading_with_arrow text-center">
            <h3>Discounted sale {{count($saleItems)}}</h3>
        </div>
        <div class="common_max_width">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="home_product_slider3" class="common_slide owl-carousel owl-theme">
                            @foreach($saleItems as $k=>$saleItem)
                                <div class="product_with_text_inner">
                                    <a  href="{{ route('item_details_page', ['item' => $saleItem->slug]) }}">
                                   <span class="product_with_quick_view">
                                       @if (sizeof($saleItem->images) > 0)
                                           <img src="{{ (!$is_customer_logged_in) ? $defaultItemImage_path : asset($saleItem->images[0]->list_image_path) }}"
                                                alt="{{ (!$is_customer_logged_in) ? config('app.name') : $saleItem->style_no }}"
                                                class="img-fluid">
                                       @else
                                           <img src="{{ ( ! $is_customer_logged_in ) ? $defaultItemImage_path : asset('images/no-image.png') }}" alt="{{ (!auth()->user()) ? config('app.name') : $saleItem->style_no }}" class="img-fluid">
                                       @endif
                                       <span class="show_quick_view"
                                             data-url="{{ route('item_details_page', ['item' => $saleItem->id]) }}"
                                             data-pid="{{$saleItem->id}}">QUICK VIEW</span>
                                    </span>
                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                            <h2 class="product_price">
                                                @if ($saleItem->orig_price != null)
                                                    <del>
                                                        ${{ number_format($saleItem->orig_price, 2, '.', '') }}</del>
                                                @endif
                                                ${{ sprintf('%0.2f', $saleItem->price) }}
                                            </h2>
                                        @endif
                                        <p class="product_title"> {{ $saleItem->name }}</p>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product_view_more text-right">
                            <a href="{{route('discounted_page')}}">View More +</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
<!-- =========================================
END Sale PRODCUT THUMB WITH TEXT SECTION
========================================== -->
<!-- =========================
START INSTAGRAM SECTION
============================== -->
<section class="instagram_area">
    <div class="common_max_width">

        @include('social.instagram')

    </div>
</section>
<!-- =========================
    END INSTAGRAM SECTION
============================== -->
<!-- =========================
START ITEM QUICK VIEW MODAL
============================== -->

<div class="modal fade bd-example-modal-lg common_popup_content " id="quickViewArea"
     tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg main_product_pop_up"
         role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1 order-md-first product-img-box">

                        </div>
                        <div class="col-sm-6 no-padding-left">
                            <div class="product_pop_up_left">
                                <div class="single_product_slide">
                                    <div id="s_pop_up_slider" class="common_slide owl-carousel owl-theme">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 no-padding-right">
                            <div class="product_pop_up_right">
                               <h2 class="product-title"></h2>
                                <p class="product_price"></p>
                                <div class="single_product_desc modal_color_table">

                                </div>
                                <p>
                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <button class="add_cart_btn" id="btnAddToCart"><i
                                                    class="icon-bag"></i> Add to Shopping Bag
                                        </button>

                                        <a href="{{ route('show_cart') }}"
                                           class="add_cart_btn btn btn-secondary"> Checkout</a>
                                    @else
                                        <a href="/login" class="add_cart_btn btn btn-primary">Login
                                            to Add to Cart</a>
                                    @endif
                                </p>
                                <div class="common_accordion product_left_accordion">
                                    <div id="INFORMATION">
                                        <div class="card">
                                            <div class="card-header">
                                                <button class="btn-link collapsed"
                                                        data-toggle="collapse"
                                                        data-target="#INFORMATIONOne">
                                                    PRODUCT INFORMATION
                                                </button>
                                            </div>
                                            <input type="hidden" id="itemInPack">
                                            <input type="hidden" id="itemPrice">
                                            <input type="hidden" id="modalItemId">
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <input type="hidden" id="loggedIn" value="true">
                                            @else
                                                <input type="hidden" id="loggedIn"
                                                       value="false">
                                            @endif
                                            <div id="INFORMATIONOne" class="collapse">
                                                <div class="card-body clearfix">
                                                    <h2>Details</h2>

                                                    <p class="text-xs description">
                                                        Description</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="text-medium stock_item_preview">Stock:</span>

                                <div class="product-sku">
                                    <span class="text-medium">Style#:</span>
                                    <span class="product_style_sku"></span>
                                </div>
                                <p class="see_full_details" >
                                    <a  class="itemDetails" href="http://andthewhy.com" >VIEW FULL DETAILS</a>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
END ITEM QUICK VIEW MODAL
============================== -->

<!-- =========================
       START HOME PRODUCT SLIDER SECTION
   ============================== -->


<!-- =========================
            END HOME PRODUCT SLIDER SECTION
        ============================== -->
@stop
@section('additionalJS')
    <script>
        $(function () {
            var sliders = <?php echo json_encode($mainSliderImages)?>;

            if (sliders[0].color == 'white')
                $('body').addClass('white_slide');

            $('.slider').on('afterChange', function (event, slick, currentSlide) {
                if (sliders[currentSlide].color == 'white') {
                    $('body').addClass('white_slide');
                } else {
                    $('body').removeClass('white_slide');
                }
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    @include('pages.quick_item_js')
@stop