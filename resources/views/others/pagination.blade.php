<div class="product_pagination">
    <ul>

        @if ($paginator->hasPages())

            @if ($paginator->onFirstPage())
                <li><a class="disabled" href="#"><img src="{{ asset('themes/andthewhy/images/product/icon_prev.svg')}}"></a>
                </li>
            @else
                <li><a class="page-url" href="{{ $paginator->previousPageUrl() }}"><img
                                src="{{ asset('themes/andthewhy/images/product/icon_prev.svg')}}"></a></li>
            @endif

            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li>...</li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a href="#">{{ $page }}</a></li>
                        @else
                            <li><a class="page-url" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <li><a class="page-url" href="{{ $paginator->nextPageUrl() }}"><img
                                src="{{ asset('themes/andthewhy/images/product/icon_next.svg')}}" alt=""></a></li>
            @else
                <li><a href="#" class="disabled"><img src="{{ asset('themes/andthewhy/images/product/icon_next.svg')}}"
                                                      alt=""></a></li>
            @endif
        @endif
    </ul>
</div>
{{--<div class="view_product_sorting">
    <div class="custom_select">
        <select>
            <option>VIEW 30</option>
            <option>VIEW 60</option>
            <option>VIEW 90</option>
            <option>VIEW 120</option>
        </select>
    </div>
</div>--}}

