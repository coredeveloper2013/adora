<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($categories as $category)
        <url>
            <loc>{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($category->name)]) }}</loc>
            <lastmod>{{ $category->updated_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.6</priority>
        </url>

        @if (sizeof($category->subCategories) > 0)
            @foreach ($category->subCategories as $sub)
                <url>
                    <loc>{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($category->name)]) }}</loc>
                    <lastmod>{{ $sub->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                    <changefreq>monthly</changefreq>
                    <priority>0.6</priority>
                </url>

                @if (sizeof($sub->subCategories) > 0)
                    @foreach ($sub->subCategories as $sub2)
                        <url>
                            <loc>{{ route('third_category', ['category' => changeSpecialChar($sub2->name), 'second' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($category->name)]) }}</loc>
                            <lastmod>{{ $sub2->updated_at->tz('UTC')->toAtomString() }}</lastmod>
                            <changefreq>monthly</changefreq>
                            <priority>0.6</priority>
                        </url>
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
</urlset>