@extends('admin.layouts.main')

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Last Updated</th>
                <th>Company Name</th>
                <th>Details</th>
                <th>Amount</th>
            </tr>
            </thead>

            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ date('F d, Y g:i:s a', strtotime($order['updated_at'])) }}</td>
                    <td>{{ $order['company']}}</td>
                    <td>
                        <a class="text-primary" href="{{ route('admin_incomplete_order_detail', ['order' => $order['user_id']]) }}">View Detail</a>
                    </td>
                    <td>${{ sprintf('%0.2f', $order['total']) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="pagination">
        {{ $result->links() }}
    </div>
@stop