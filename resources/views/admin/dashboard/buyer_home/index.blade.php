@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin_buyer_home_save') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <textarea class="ckeditor" name="buyer_home" id="buyer_home" rows="10" cols="80">{{ $setting->value or '' }}</textarea>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary" id="btnOrderNoticeSubmit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ url('laravel-filemanager') }}type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };

            CKEDITOR.replace('buyer_home', options);
        });
    </script>
@stop