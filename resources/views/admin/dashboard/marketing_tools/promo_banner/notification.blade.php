@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <form action="{{ route('notification_add', ['id' => $page->page_id]) }}" method="POST">
        @csrf

        <div class="row">
            <div class="col-md-12">
                <textarea class="d-none" name="page_editor" id="page_editor" rows="2">{{ $page->content }}</textarea>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-12 text-right">
                <input class="btn btn-primary" type="submit" value="SAVE">
            </div>
        </div>
    </form>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('plugins/ckeditor/ckeditor.js') }}?id={{ rand() }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            //var pageEditor = CKEDITOR.replace( 'page_editor' );

            var options = {
                filebrowserImageBrowseUrl: '{{ url('laravel-filemanager') }}?type=Images',
                filebrowserImageUploadUrl: '{{ url('laravel-filemanager') }}type=Images&_token=',
                filebrowserBrowseUrl: '{{ url('laravel-filemanager') }}?type=Files',
                filebrowserUploadUrl: '{{ url('laravel-filemanager') }}?type=Files&_token='
            };

            CKEDITOR.replace('page_editor', options);
        });
    </script>
@stop