@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/admire/css/components.css') }}" rel="stylesheet">
    <style type="text/css">
        input.form-control {
    display: block;
    width: 100%;
    border: 0;
    background: #fff;
    border: 1px solid #999;
    padding: 4px 15px;
    font-size: 14px;
    line-height: 18px;
    color: #343434;
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-progress-appearance: none;
    border-radius: 0;
    margin-bottom: 5px;
    -webkit-border-radius: 0;
}
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">Add / Update Social Feeds</span></h3>

            @if ( session()->has('message') )
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif

            <form class="form-horizontal" id="form" method="post" action="{{ route('admin_social_feed_add_post') }}">
                @csrf
                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Facebook Feeds : </label>
                    </div>

                    <div class="col-lg-5">
                        <input type="hidden" name="facebook" value="facebook">
                        <input type="text" id="" class="form-control" placeholder="Access Token" name="facebook_access_token" value="<?php echo $data_array['facebook']['access_token']?>">
                        <input type="text" id="" class="form-control" placeholder="User Token" name="facebook_user_token" value="<?php echo $data_array['facebook']['user_token']?>">
                        <input type="text" id="" class="form-control" placeholder="Pass Token" name="facebook_pass_token" value="<?php echo $data_array['facebook']['pass_token']?>">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Instagram Feeds : </label>
                    </div>

                    <div class="col-lg-5">
                        <input type="hidden" name="instagram" value="instagram">
                        <input type="text" id="" class="form-control" placeholder="Access Token" name="instagram_access_token" value="<?php echo $data_array['instagram']['access_token']?>">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Twitter Feeds : </label>
                    </div>

                    <div class="col-lg-5">
                        <input type="hidden" name="twitter" value="twitter">
                        <input type="text" id="" class="form-control" placeholder="Access Token" name="twitter_access_token" value="<?php echo $data_array['twitter']['access_token']?>">
                        <input type="text" id="" class="form-control" placeholder="User Token" name="twitter_user_token" value="<?php echo $data_array['twitter']['user_token']?>">
                        <input type="text" id="" class="form-control" placeholder="Pass Token" name="twitter_poass_token" value="<?php echo $data_array['twitter']['pass_token']?>">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="Save or update">
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('additionalJS')
    
@stop