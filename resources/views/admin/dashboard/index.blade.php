@extends('admin.layouts.main')

@section('additionalCSS')

@stop

@section('content')
    <div class="dashboard_home">
        <div class="item_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="sub_title">
                            <h2>My Performance <span>Last 30 days</span></h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="performance_inner">
                            <div class="performance_icon bg_primary">
                                <i class="fa fa-eye"></i>
                            </div>
                            <p>Home Page Visitor</p>
                            <h2>{{ number_format($data['homePageVisitor']) }}</h2>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="performance_inner">
                            <div class="performance_icon bg_danger">
                                <i class="fa fa-eye"></i>
                            </div>
                            <p>Home Page Visitor (Unique)</p>
                            <h2>{{ number_format($data['homePageVisitorUnique']) }}</h2>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="performance_inner">
                            <div class="performance_icon bg_info">
                                <i class="fa fa-cart-plus"></i>
                            </div>
                            <p>Total Sale Amount</p>
                            <h2>${{ number_format($data['totalSaleAmount'], 2, '.', ',') }}</h2>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="performance_inner">
                            <div class="performance_icon bg_warning">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                            <p>Total Pending Order</p>
                            <h2>${{ number_format($data['totalPendingOrder'], 2, '.', ',') }}</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="sub_title">
                            <h2>Additional Stats</h2>
                        </div>
                    </div>
                </div>
                <div class="row performance_transparent_icon">
                    <div class="col">
                        <div class="performance_inner">
                            <div class="performance_icon">
                                <i class="fas fa-dollar-sign"></i>
                            </div>
                            <h2>${{ number_format($data['todayOrderAmount'], 2, '.', ',') }}</h2>
                            <p>Today Order Amount</p>
                        </div>
                    </div>
                    <div class="col">
                        <div class="performance_inner">
                            <div class="performance_icon">
                                <i class="fa fa-eye"></i>
                            </div>
                            <h2>{{ number_format($data['yesterdayVisitor']) }}</h2>
                            <p>Yesterday Visitor</p>

                        </div>
                    </div>
                    <div class="col">
                        <div class="performance_inner">
                            <div class="performance_icon">
                                <i class="fa fa-upload"></i>
                            </div>
                            <h2>${{ number_format($data['yesterdayOrderAmount'], 2, '.', ',') }}</h2>
                            <p>Yesterday Order Amount</p>

                        </div>
                    </div>
                    <div class="col">
                        <div class="performance_inner">
                            <div class="performance_icon">
                                <i class="fa fa-upload"></i>
                            </div>
                            <h2>{{ number_format($data['itemUploadedThisMonth']) }}</h2>
                            <p>Item Uploaded (This Month)</p>

                        </div>
                    </div>
                    <div class="col">
                        <div class="performance_inner">
                            <div class="performance_icon">
                                <i class="fa fa-upload"></i>
                            </div>
                            <h2>{{ number_format($data['itemUploadedLastMonth']) }}</h2>
                            <p>Item Uploaded (This Month)</p>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="sub_title">
                            <h2>Top 6 Styles</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @foreach($bestItems as $item)
                            <div class="home_product_inner">
                                <a href="{{ route('admin_edit_item', ['item' => $item->id]) }}"><img src="{{ $item->image_path }}" alt="" class="img-fluid"></a>
                                <p><a href="{{ route('admin_edit_item', ['item' => $item->id]) }}">{{ $item->style_no }}</a></p>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <h4>Total Order Count by Month</h4>
                        <hr>

                        <canvas id="orderCount" height="200"></canvas>
                    </div>

                    <div class="col-6">
                        <h4>Item Uploaded by Month</h4>
                        <hr>

                        <canvas id="myChart2" height="200"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('additionalJS')
    <script src="{{ asset('plugins/Chart.js/Chart.min.js') }}"></script>
    <script>
        var orderCountLabel = <?php echo $data['orderCountLabel']; ?>;
        var orderCount = <?php echo $data['orderCount']; ?>;
        var returnOrder = <?php echo $data['returnOrder']; ?>;
        var uploadCount = <?php echo $data['uploadCount']; ?>;

        var chartOrderCount = document.getElementById("orderCount").getContext('2d');
        var chartOrderCount = new Chart(chartOrderCount, {
            type: 'bar',
            data: {
                labels: orderCountLabel,
                datasets: [{
                    label: 'New',
                    data: orderCount,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor:  'rgba(255,99,132,1)',
                    borderWidth: 1
                },
                    {
                        label: 'Return',
                        data: returnOrder,
                        backgroundColor: 'rgba(255, 206, 86, 0.2)',
                        borderColor: 'rgba(255, 206, 86, 1)',
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            max: Math.max(Math.max(...orderCount) + 5, Math.max(...returnOrder) + 5),
        }
        }]
        }
        }
        });

        var ctx = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: orderCountLabel,
                datasets: [{
                    data: uploadCount,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor:  'rgba(255,99,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            max: Math.max(...uploadCount) + 5
        }
        }]
        }
        }
        });
    </script>
@stop