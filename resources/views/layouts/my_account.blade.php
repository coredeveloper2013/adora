<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $meta_title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $meta_description }}" />

    <!-- Favicon -->
    {{--<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">--}}
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/menuzord.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/lightslider.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/main.css') }}">
    @yield('additionalCSS')
</head>

<body class="product_page my_account_page">
    <!-- Header -->
    @include('layouts.shared.header')
    <!-- Header -->
    @include('layouts.shared.left_menu')
    <!-- Content -->
    <section class="my_account_area">
        @yield('content')
    </section>
    <!-- Content -->

    <!-- Footer -->
    @include('layouts.shared.footer')
    <!-- Footer -->

    <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
        {{ csrf_field() }}
    </form>


    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="{{ asset('themes/andthewhy/js/vendor/jquery-1.11.2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{ asset('themes/andthewhy/js/vendor/bootstrap.js') }}"></script>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"></script>
    <script src="{{ asset('themes/andthewhy/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/menuzord.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/lightslider.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/jquery.bootFolio.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/main.js') }}"></script>
    <script>
        $(function () {
            $('.btnLogOut').click(function () {
                $('#logoutForm').submit();
            });
        });
    </script>
    @yield('additionalJS')
</body>
</html>