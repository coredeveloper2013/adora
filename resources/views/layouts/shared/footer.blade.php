<!-- =========================
            START FOOTER SECTION
        ============================== -->
<footer class="footer_area">
    <div class="footer_signup">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer_signup_inner text-center">
                        <h2>Fear of missing out?</h2>

                        <p>Be the first to know about the latest deals, style updates & more!</p>

                        <p id="mail_chimp_message" class="alert"></p>
                        <div class="footer_signup_inner_form">


                            <input type="email" name="mail_to_add" id="mail_to_add" class="form-control" placeholder="Email Address" required>
                            <input type="hidden" id="this_site_url" value="{{ route('add_email_to_mailchimp')  }}">
                            {{--<label>EMAIL ADDRESS</label>--}}
                            <button type="submit" id="mailchimp_add">JOIN</button>
                        </div>
                        <div class="footer_signup_popup">
                            <p>By clicking join, I accept the <a href="{{route('privacy_policy')}}"
                                                                 >Privacy Policy</a> and <a
                                        href="{{route('terms_conditions')}}" >Terms of Use.</a> You
                                may unsubscribe at any time</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_menu">
        <div class="container">
            <div class="row">
                {{--<div class="col">--}}
                    {{--<div class="footer_menu_inner">--}}
                        {{--<h2>Brands</h2>--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">Riley Rose</a></li>--}}
                            {{--<li><a href="#">Forever21</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>Company Info</h2>
                        <ul>
                            <li><a href="{{ route('about_us') }}">About Us</a></li>
                            <li><a href="{{ route('privacy_policy')  }}">Privacy Policy</a></li>
                            <li><a href="{{ route('terms_conditions')  }}">Terms & Conditions</a></li>


                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>Help</h2>
                        <ul>
                            <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            <li><a href="{{ route('cookies_policy')  }}">Cookies Policy</a></li>
                            <li><a href="{{ route('return_info')  }}">Return Info</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>Quick Links</h2>
                        {{--<ul>--}}
                            {{--<li><a href="#">Special Offers</a></li>--}}
                            {{--<li><a href="#">Gift Cards</a></li>--}}
                            {{--<li><a href="#">Blog</a></li>--}}
                            {{--<li><a href="#">F21 RED</a></li>--}}
                            {{--<li><a href="{{ route('privacy_policy') }}">Privacy Policy</a></li>--}}
                            {{--<li><a href="#">Your California Privacy Rights</a></li>--}}
                            {{--<li><a href="#">CA Transparency in Supply Chains Act</a></li>--}}
                            {{--<li><a href="{{ route('terms_conditions') }}">Terms of Use</a></li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col">
                    <div class="footer_menu_inner">
                        <h2>BUSINESS HOUR</h2>
                        <ul>
                            <li>Mon - Fri: 8AM - 6PM</li>
                            <li>Sat: Closed</li>
                            <li>Sun: Closed</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_social">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer_social_inner">
                        <ul>
                            @if ( isset($social_links->facebook) && $social_links->facebook != '' )
                                <li><a href="{{ $social_links->facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                            @endif
                            @if ( isset($social_links->twitter) && $social_links->twitter != '' )
                                <li><a href="{{ $social_links->twitter }}"><i class="fab fa-twitter"></i></a></li>
                            @endif
                            @if ( isset($social_links->pinterest) && $social_links->pinterest != '' )
                                <li><a href="{{ $social_links->pinterest }}"><i class="fab fa-pinterest-p"></i></a></li>
                            @endif
                            @if ( isset($social_links->instagram) && $social_links->instagram != '' )
                                <li><a href="{{ $social_links->instagram }}"><i class="fab fa-instagram"></i></a></li>
                            @endif
                            @if ( isset($social_links->google_plus) && $social_links->google_plus != '' )
                                <li><a href="{{ $social_links->google_plus }}"><i class="fab fa-google"></i></a></li>
                            @endif
                            @if ( isset($social_links->whatsapp) && $social_links->whatsapp != '' )
                                <li><a href="{{ $social_links->whatsapp }}"><i class="fab fa-whatsapp"></i></a></li>
                            @endif
                            {{--<li class="hide_mobile"><a href="#"><img--}}
                                            {{--src="{{ asset('themes/andthewhy/images/apple.png') }}" alt=""></a></li>--}}
                            {{--<li class="hide_mobile"><a href="#"><img--}}
                                            {{--src="{{ asset('themes/andthewhy/images/google.png') }}" alt=""></a></li>--}}
                        </ul>
                        <ul class="show_mobile">
                            <li><a href="#"><img src="{{ asset('themes/andthewhy/images/apple.png') }}" alt=""></a></li>
                            <li><a href="#"><img src="{{ asset('themes/andthewhy/images/google.png') }}" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer_copyright text-center">
                        <p>© {{date('Y')}} {{env('APP_SITE')}}. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- =========================
    END FOOTER SECTION
============================== -->

<a href="javascript:" id="return-to-top"><i class="fas fa-arrow-up"></i></a>


<!-- Modal -->
<div class="modal fade bd-example-modal-lg common_popup_content" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <h5 class="modal-title" id="exampleModalLabel">Privacy Policy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg common_popup_content" id="exampleModal1" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Terms of Use</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt sit sint temporibus laudantium
                    perferendis quas saepe, ad, sapiente consequuntur tempora magnam cupiditate! Autem beatae sed, quasi
                    id corrupti fugit eius.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti amet harum
                    eveniet eligendi et hic, dignissimos ipsa temporibus, voluptas recusandae assumenda, cum maxime
                    facere odio quod doloribus fugiat soluta labore?Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit. Distinctio, impedit odit, ex eligendi commodi deleniti esse accusamus laboriosam modi sequi
                    quibusdam quos inventore enim pariatur dolorum nemo nisi culpa quae!Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Aliquam assumenda officiis, laborum quidem omnis rerum natus est,
                    fuga, magnam nesciunt consectetur aliquid iste voluptate corporis modi sit eveniet explicabo
                    eos.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum eveniet odio delectus nobis
                    harum dolorem optio quas expedita dolorum excepturi, ipsam, ea at, minus vel. Itaque libero in
                    deserunt modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem expedita
                    voluptatum quod sunt assumenda, temporibus repudiandae est eum. Inventore labore pariatur, debitis
                    magni adipisci consectetur cum! Magnam fugiat, quibusdam repudiandae.Lorem ipsum dolor sit amet,
                    consectetur adipisicing elit. Unde facilis, molestiae vero. Rerum id fuga enim, labore, porro in!
                    Laborum iste saepe tempore sequi cum accusantium reiciendis quis animi. Asperiores?</p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-md common_popup_content_small" id="see_details" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <p>Receive up to 70% off select styles at Forever21.com and participating Forever 21 locations from
                    January 1, 2018 12:00 am PST through January 7, 2018 11:59 pm PST. Offer not valid on purchases of
                    gift cards or e-gift cards, applicable taxes, or shipping and handling charges. Offer is not
                    transferable and not valid for cash or cash equivalent. No adjustments on previous purchases.
                    Nothing stated herein will affect customers' legal rights. Forever 21 reserves the right to modify
                    or cancel this promotion at any time without notice</p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-md common_popup_content_small" id="see_details2" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body text-center">
                <p>Receive 30% off your purchase of regular priced lingerie items at forever21.com and participating
                    Forever 21 locations only from January 1, 2019 at 12:00am PST through January 6, 2019 11:59pm PST.
                    Offer valid only on items marked “30% off”. While supplies last. Offer cannot be combined with any
                    other promotion, offer, or discount. Not valid on the purchase of gift cards or e-gift cards,
                    non-Forever 21 branded items, Core Basics, Red Deals, taxes, shipping and handling charges,
                    discounted, promotional, or sale items. No adjustments on previous purchases. Not redeemable for
                    cash or cash equivalent. Nothing stated herein will affect customer’s legal rights. Forever 21
                    reserves the right to modify or cancel this promotion at any time without notice. Receive 30% off
                    your purchase of select beauty items at forever21.com and participating Forever 21 locations only
                    from January 1, 2018 at 12:00am PST through January 6, 2018 11:59pm PST. Offer valid only on items
                    marked “30% off”. While supplies last. Offer cannot be combined with any other promotion, offer, or
                    discount. Not valid on the purchase of gift cards or e-gift cards, non-Forever 21 branded items,
                    Core Basics, Red Deals, taxes, shipping and handling charges, discounted, promotional, or sale
                    items. No adjustments on previous purchases. Not redeemable for cash or cash equivalent. Nothing
                    stated herein will affect customer’s legal rights. Forever 21 reserves the right to modify or cancel
                    this promotion at any time without notice.</p>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg common_popup_content_small" id="instamodal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding-left">
                            <div class="insta_pop_up_left">
                                <img src="{{ asset('themes/andthewhy/images/instagram/insta1.png') }}" alt=""
                                     class="img-fluid">

                                <div class="insta_popup_info">
                                    <img src="{{ asset('themes/andthewhy/images/instagram/insta-logo.jpg') }}" alt="">

                                    <h2>@forever21</h2>

                                    <p>2018-12-23</p>

                                    <p>Season it with some sparkles. ✨ Get your last-minute NYE looks for 30% off now!
                                        Use code: HOLIDAY30 💛 Make sure to tag @forever21 + #F21xMe for a chance to be
                                        featured like #ForeverBabe @jamialix xo -- Search: 00318834</p>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 no-padding-right">
                            <div class="insta_pop_up_right">
                                <h1>SHOP IT (1 item)</h1>
                                <a href="#">
                                    <img src="{{ asset('themes/andthewhy/images/instagram/insta11.png') }}" alt=""
                                         class="img-fluid">

                                    <h2>$28</h2>

                                    <p>Iridescent Sequin Cowl Neck Dress</p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->

<!-- Modal -->
{{--<div class="modal fade bd-example-modal-lg common_popup_content " id="quickview" tabindex="-1" role="dialog"--}}
     {{--aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
    {{--<div class="modal-dialog modal-dialog-centered modal-lg main_product_pop_up" role="document">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header clearfix">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--<img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">--}}
                {{--</button>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}
                {{--<div class="container-fluid">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-1 order-md-first">--}}
                            {{--<ul class="single_product_left_thumbnail">--}}
                                {{--<li>--}}
                                    {{--<img src="{{ asset('themes/andthewhy/images/product/product-50.jpg') }}"--}}
                                         {{--class="img-fluid" alt="">--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<img src="{{ asset('themes/andthewhy/images/product/product-51.jpg') }}"--}}
                                         {{--class="img-fluid" alt="">--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<img src="{{ asset('themes/andthewhy/images/product/product-52.jpg') }}"--}}
                                         {{--class="img-fluid" alt="">--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<img src="{{ asset('themes/andthewhy/images/product/product-45.jpg') }}"--}}
                                         {{--class="img-fluid" alt="">--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6 no-padding-left">--}}
                            {{--<!-- <img src="{{ asset('themes/andthewhy/images/product/product-41.jpg') }}" alt="" class="img-fluid"> -->--}}
                            {{--<div class="product_pop_up_left">--}}
                                {{--<div class="single_product_slide">--}}
                                    {{--<div id="s_pop_up_slider" class="common_slide owl-carousel owl-theme">--}}
                                        {{--<div class="single_product_slide_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-50.jpg') }}"--}}
                                                     {{--alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="single_product_slide_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-51.jpg') }}"--}}
                                                     {{--alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="single_product_slide_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-52.jpg') }}"--}}
                                                     {{--alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="single_product_slide_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-45.jpg') }}"--}}
                                                     {{--alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-5 no-padding-right">--}}
                            {{--<div class="product_pop_up_right">--}}
                                {{--<h2>Fantasy Graphic Crop Top</h2>--}}

                                {{--<p><b>$12.90</b></p>--}}
                                {{--<!-- <ul class="link_to_slide">--}}
                                    {{--<li><button><img src="{{ asset('themes/andthewhy/images/smp2.jpg') }}" alt="" class="img-fluid"></button></li>--}}
                                    {{--<li><button><img src="{{ asset('themes/andthewhy/images/smp2.jpg') }}" alt="" class="img-fluid"></button></li>--}}
                                    {{--<li><button><img src="{{ asset('themes/andthewhy/images/smp2.jpg') }}" alt="" class="img-fluid"></button></li>--}}
                                {{--</ul>--}}
                                {{--<p>WHITE/BLACK</p>--}}
                                {{--<ul class="product_size_desc">--}}
                                    {{--<li class="active"><button>S</button></li>--}}
                                    {{--<li><button>M</button></li>--}}
                                    {{--<li><button>L</button></li>--}}
                                {{--</ul> -->--}}
                                {{--<div class="single_product_desc">--}}
                                    {{--<table class="table">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>Color</th>--}}
                                            {{--<th>S-M-L</th>--}}
                                            {{--<th>Pack</th>--}}
                                            {{--<th>Qty</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td>GOLD</td>--}}
                                            {{--<td>2-2-2</td>--}}
                                            {{--<td><input type="text"></td>--}}
                                            {{--<td>0</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>SILVER</td>--}}
                                            {{--<td>2-2-2</td>--}}
                                            {{--<td><input type="text"></td>--}}
                                            {{--<td>0</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td colspan="3"><b>Total</b></td>--}}
                                            {{--<td><b>0</b></td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                                {{--<p>--}}
                                    {{--<button class="add_cart_btn">add to cart</button>--}}
                                {{--</p>--}}
                                {{--<div class="common_accordion product_left_accordion">--}}
                                    {{--<div id="INFORMATION">--}}
                                        {{--<div class="card">--}}
                                            {{--<div class="card-header">--}}
                                                {{--<button class="btn-link collapsed" data-toggle="collapse"--}}
                                                        {{--data-target="#INFORMATIONOne">--}}
                                                    {{--PRODUCT INFORMATION--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                            {{--<div id="INFORMATIONOne" class="collapse">--}}
                                                {{--<div class="card-body clearfix">--}}
                                                    {{--<h2>Details</h2>--}}

                                                    {{--<p>A fleece knit hoodie featuring a Los Angeles Lakers™ team logo--}}
                                                        {{--graphic, a drawstring hood, dropped long sleeves, a raw-cut hem,--}}
                                                        {{--and cropped boxy silhouette.</p>--}}

                                                    {{--<h2>Content + Care</h2>--}}
                                                    {{--<ul>--}}
                                                        {{--<li>57% cotton, 43% polyester</li>--}}
                                                        {{--<li>- Machine wash cold</li>--}}
                                                    {{--</ul>--}}
                                                    {{--<h2>Size + Fit</h2>--}}
                                                    {{--<ul>--}}
                                                        {{--<li>- Model is 5'9" and wearing a Small</li>--}}
                                                        {{--<li>- Machine wash cold</li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<p class="no_padding_btm">Product Code : 2000335014</p>--}}

                                {{--<p class="no_padding_btm">SKU : 00335014014</p>--}}

                                {{--<p class="see_full_details"><a href="#">VIEW FULL DETAILS</a></p>--}}

                                {{--<!-- <div class="similiar_item">--}}
                                    {{--<p><b>SIMILAR LOOKS</b></p>--}}
                                    {{--<div class="similiar_item_wrapper">--}}
                                    {{--<div id="similiar_item_slide" class="common_slide owl-carousel owl-theme">--}}
                                        {{--<div class="similiar_item_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-50.jpg') }}" alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="similiar_item_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-51.jpg') }}" alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="similiar_item_inner">--}}
                                            {{--<a href="#">--}}
                                                {{--<img src="{{ asset('themes/andthewhy/images/product/product-52.jpg') }}" alt="" class="img-fluid">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div> -->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<!-- Modal -->
