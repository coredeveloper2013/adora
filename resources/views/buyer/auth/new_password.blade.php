@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="content col-md-6 margin-bottom-3x margin-top-3x">
                <h3>New Password</h3>
                <p>Please enter new password</p>
                <form class="login-box" method="post" action="{{ route('new_password_post_buyer') }}">
                    @csrf

                    <div class="form-group input-group">
                        <input class="form-control" type="password" placeholder="Password" name="password" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                    </div>

                    <div class="form-group input-group">
                        <input class="form-control" type="password" placeholder="Re-enter Password" name="password_confirmation" required><span class="input-group-addon"><i class="icon-lock"></i></span>
                    </div>

                    @if ($errors->has('password'))
                        <div class="form-group">
                            <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="form-control-feedback">{{ session('message') }}</div>
                    </div>

                    <input type="hidden" name="token" value="{{ request()->get('token') }}">

                    <div class="text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit">Reset Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop