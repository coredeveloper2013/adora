<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/menuzord.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/lightslider.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/main.css') }}">
</head>
<body class="login_page">
<!-- Header -->
@include('layouts.shared.header')
<div class="discount_area clearfix common_top_margin">
    <div class="container common_container">
        <div class="row">
            <div class="col-md-6">
                <div class="discount_inner text-center">
                    <a href="#">
                        <h2>THE WINTER SALE 50% OFF SELECT OUTERWEAR &amp; ACCESSORIES </h2>
                        <p>SHOP NOW  <span data-toggle="modal" data-target="#see_details">*see details</span></p>
                    </a>
                </div>
            </div>
            <div class="col-md-6 show_desktop">
                <div class="discount_inner discount_inner_last text-center">
                    <a href="#">
                        <h2>THE WINTER SALE 50% OFF SELECT OUTERWEAR &amp; ACCESSORIES </h2>
                        <p>SHOP NOW  <span data-toggle="modal" data-target="#see_details2">*see details</span></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header -->
<div class="common_bredcrumbs">
    <div class="container common_container">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Business With Us</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="company_info_area">
    <div class="container common_container">
        <div class="row">
            <div class="col-md-2">
                <div class="company_info_list show_desktop">
                    <h2>COMPANY INFO</h2>
                    <ul>
                        <li class="active"><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Affiliate Program</a></li>
                        <li><a href="#">Social Responsibility</a></li>
                        <li><a href="#">Business With Us</a></li>
                        <li><a href="#">Press &amp; Talent</a></li>
                        <li><a href="#">Find a Store</a></li>
                        <li><a href="#">Newsroom</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
                <div class="shop_filter_inner show_mobile">
                    <div class="custom_select" style="width:100%">
                        <select>
                            <option value="0">About Us</option>
                            <option value="0">Careers</option>
                            <option value="0">Affiliate Program</option>
                            <option value="0">Social Responsibility</option>
                            <option value="0">Business With Us</option>
                            <option value="0">Press &amp; Talent</option>
                            <option value="0">Find a Store</option>
                            <option value="0">Newsroom</option>
                            <option value="0">Site Map</option>
                        </select>
                        <div class="select-selected">About Us</div><div class="select-items select-hide"><div>Careers</div><div>Affiliate Program</div><div>Social Responsibility</div><div>Business With Us</div><div>Press &amp; Talent</div><div>Find a Store</div><div>Newsroom</div><div>Site Map</div></div></div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="about_us_content">
                    <div class="show_desktop">
                        <h2 class="company_info_title ">BUSINESS WITH US</h2>
                    </div>

                    <div class="row">
                        <div class="col-md-7">
                            <div class="companyinfo_content_text business_us_content">
                                @if (session('message'))
                                    <div class="alert alert-danger">
                                        {{ session('message') }}
                                    </div>
                                @endif

                                <form method="post" action="{{route('reset_password_buyer_now' , ['token' => $token])}}">
                                    @csrf
                                    <div class="form-group">
                                        {{--<label>Email *</label>--}}
                                        <span class="text-danger">{{$errors->has('password') ? $errors->first('password') : ''}}</span>
                                        <input type="password" name="password" class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        {{--<label>Email *</label>--}}
                                        <span class="text-danger">{{$errors->has('confirm_password') ? $errors->first('confirm_password') : ''}}</span>
                                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                                    </div>
                                    <div class="form-group business_form">
                                        <button>Reset Password</button>
                                    </div>
                                </form>
                            </div>
                        </div>


                        <div class="col-md-5 show_desktop">
                            <div class="about_us_content_img">
                                <div class="login_inner ">
                                    <div class="login_inner_form">
                                        <h2>I want a CQBYCQ user account</h2>
                                        <p>If you still don't have a <b>CQBYCQ.com</b> account, use this option to access the registration form.</p>
                                        <p>Provide your details to make <b>CQBYCQ.com</b> purchases easier.</p>
                                        <a href="{{ route('buyer_register') }}" class="btn btn-primary">CREATE ACCOUNT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content -->

<!-- Content -->

<!-- Footer -->
@include('layouts.shared.footer')
<!-- Footer -->
<!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
<script src="{{ asset('themes/andthewhy/js/vendor/jquery-1.11.2.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{ asset('themes/andthewhy/js/vendor/bootstrap.js') }}"></script>
<script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"></script>
<script src="{{ asset('themes/andthewhy/js/owl.carousel.js') }}"></script>
<script src="{{ asset('themes/andthewhy/js/menuzord.js') }}"></script>
<script src="{{ asset('themes/andthewhy/js/lightslider.js') }}"></script>
<script src="{{ asset('themes/andthewhy/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('themes/andthewhy/js/jquery.bootFolio.js') }}"></script>
<script src="{{ asset('themes/andthewhy/js/main.js') }}"></script>
</body>
</html>
