@if(count($social_feeds) > 0)
    @foreach($social_feeds as $feed)
<div class="modal fade bd-example-modal-lg common_popup_content_small" id="instamodal-{{$feed->id}}" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 no-padding-left">
                            <div class="insta_pop_up_left">
                                <img src="{{ $feed->images->standard_resolution->url }}" alt=""
                                     class="img-fluid">

                                {{--<div class="insta_popup_info">--}}
                                    {{--<img src="{{ $feed->images->standard_resolution->url }}}" alt="">--}}

                                    {{--<h2>@forever21</h2>--}}

                                    {{--<p>2018-12-23</p>--}}

                                    {{--<p>Season it with some sparkles. ✨ Get your last-minute NYE looks for 30% off now!--}}
                                        {{--Use code: HOLIDAY30 💛 Make sure to tag @forever21 + #F21xMe for a chance to be--}}
                                        {{--featured like #ForeverBabe @jamialix xo -- Search: 00318834</p>--}}
                                {{--</div>--}}

                            </div>
                        </div>
                        <div class="col-sm-6 no-padding-right">
                            <div class="insta_pop_up_right" >
                                {!!   $feed->caption->text ?  nl2br($feed->caption->text) : '' !!}
                                <hr>
                                <p style="text-align: right">
                                    <a target="_blank" href="{{ $feed->link }}">Details</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    @endforeach

@endif

@if(count($social_feeds) > 0)
    <section class="product_slider_area">

            <div class="row">
                <div class="col-md-12">
                    <h2 class="color_pink" style="text-align: center">#Instagram Post</h2>
                    <div id="instagram_slider" class="common_slide owl-carousel owl-theme">
                        @foreach($social_feeds as $feed)
                            <div class="product_slider_inner">
                                <a href="#" data-toggle="modal" data-target="#instamodal-{{$feed->id}}">
                                    <img src="{{ $feed->images->standard_resolution->url }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

    </section>
@endif